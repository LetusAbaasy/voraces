package Tablero;

public class Beneficio {

    float beneficio;
    float base;
    float altura;
    float bxp;

    public Beneficio(float base, float altura, float beneficio) {
        this.base = base;
        this.altura = altura;
        this.beneficio = beneficio;
        this.bxp = (beneficio / (base * altura));
    }

    public Beneficio() {
		// TODO Auto-generated constructor stub
	}

	public float getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(float beneficio) {
        this.beneficio = beneficio;
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float getBxp() {
        return bxp;
    }

    public void setBxp(float bxp) {
        this.bxp = bxp;
    }
}
