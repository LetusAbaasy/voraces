package Tablero;

import javax.swing.JOptionPane;

public class Tablero {

    public static void llenar(Beneficio vector[]) {
        for (int i = 0; i < vector.length; i++) {
            int ba = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la 'Base' de la pieza " + (i + 1)));
            int al = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la 'Altura' de la pieza " + (i + 1)));
            int be = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el 'Beneficio' de la pieza " + (i + 1)));
            vector[i] = new Beneficio(ba, al, be);
        }
    }

    public static void mostrar(Beneficio aux[]) {
        String pieza = "Pieza:  |", base = "Base:  |", altura = "Altura:  |", beneficio = "Beneficio:  |", valor = "____________________________\nBeneficio\nPor cuadricula:  |";
        for (int i = 0; i < aux.length; i++) {
            pieza += String.valueOf(i + 1) + " |";
            base += aux[i].getBase() + " |";
            altura += aux[i].getAltura() + " |";
            beneficio += aux[i].getBeneficio() + " |";
            valor += aux[i].getBeneficio() / (aux[i].getBase() * aux[i].getAltura()) + " |";
        }
        JOptionPane.showMessageDialog(null, "/*/*/DATOS DE LAS PIEZAS/*/*/ \n\n" + pieza + "\n" + base + "\n" + altura + "\n" + beneficio + "\n" + valor);
    }

    public static void ordenar(Beneficio[] vector) {
        int i, k, j;
        double mayor;
        Beneficio aux = new Beneficio();
        aux = vector[0];
        for (i = 0; i < vector.length; i++) {
            mayor = vector[i].getBeneficio();
            k = i;
            for (j = i + 1; j < vector.length; j++) {
                if (vector[j].getBeneficio() < mayor) {
                    mayor = vector[j].getBeneficio();
                    aux = vector[j];
                    k = j;
                }
            }
            aux = vector[k];
            vector[k] = vector[i];
            vector[i] = aux;
        }
    }

    public static void mostrarOrdenado(Beneficio aux[]) {
        String pieza = "Pieza:  |", base = "Base:  |", altura = "Altura:  |", beneficio = "Beneficio:  |", valor = "______________________________\nBeneficio\nPor cuadricula:  |";
        for (int i = 0; i < aux.length; i++) {
            pieza += String.valueOf(i + 1) + " | ";
            base += aux[i].getBase() + " | ";
            altura += aux[i].getAltura() + " | ";
            beneficio += aux[i].getBeneficio() + " | ";
            valor += aux[i].getBeneficio() / (aux[i].getBase() * aux[i].getAltura()) + " | ";
        }
        JOptionPane.showMessageDialog(null, "/*/--DATOS ORDENADOS \n"
                + "DE MENOR A MAYOR BENEFICIO--/*/ \n\n" + pieza + "\n" + base + "\n" + altura + "\n" + beneficio + "\n" + valor);
    }

    public static void operar(float solucion[], Beneficio objetos[]) {
        int b = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la 'BASE' del Tablero"));
        int a = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la 'ALTURA' del Tablero"));
        float basePieza = 0;
        float alturaPieza = 0;
        int i = solucion.length - 1;
        while (basePieza < b && alturaPieza < a || i >= 0) {

            JOptionPane.showMessageDialog(null, "Base: " + basePieza + " - Altura: " + alturaPieza + "\n_________________________________\nPieza: " + i + " Con Base: " + objetos[i].getBase() + " y Altura: " + objetos[i].getAltura());
            if (basePieza + objetos[i].getBase() <= b && alturaPieza + objetos[i].getAltura() <= a) {

                solucion[i] = objetos[i].getBeneficio();//1
                basePieza += objetos[i].getBase();
                alturaPieza += objetos[i].getAltura();
                JOptionPane.showMessageDialog(null, "Base : " + basePieza + " y Altura : " + alturaPieza + "\n____________________________\nPieza : " + i + "\nBeneficio Tomado : " + solucion[i]);

            } else {
                solucion[i] = (objetos[i].getBeneficio() / (objetos[i].getBase() * objetos[i].getAltura()));
            }
            i--;
        }
    }

    public static void main(String[] args) {
        int p = Integer.parseInt(JOptionPane.showInputDialog("Ingrese La Cantidad De Piezas"));
        float solucion[] = new float[p];
        Beneficio objetos[] = new Beneficio[p];
        llenar(objetos);
        mostrar(objetos);
        ordenar(objetos);
        mostrarOrdenado(objetos);
        operar(solucion, objetos);
    }
}
