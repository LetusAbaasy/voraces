package Votantes;

public class Probabilidad {

	float probabilidad;

	public Probabilidad() {
		// TODO Auto-generated constructor stub
	}

    public Probabilidad(float prob) {
        this.probabilidad = prob;
    }
    
    public float getProbabilidad() {
		return probabilidad;
	}

	public void setProbabilidad(float probabilidad) {
		this.probabilidad = probabilidad;
	}
}
