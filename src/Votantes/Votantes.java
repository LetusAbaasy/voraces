package Votantes;

import java.util.Arrays;

import javax.swing.JOptionPane;

import Tablero.Beneficio;

public class Votantes {
	
	public static void llenarProbabilidad(Votantes[] votantes, Candidatos[] candidatos) {
        for (int i = 0; i < votantes.length; i++) {
        	for (int j = 0; j < candidatos.length; j++) {
	            float prob = Float.parseFloat(JOptionPane.showInputDialog("Ingrese la Probabilidad del voto para el Candidato: #" + (j + 1)+ " del Votante: #" +(i + 1)+" ..."));
	            Probabilidad Probabilidad = new Probabilidad(prob);
	            System.out.println("Probabilidad: #"+i+"- "+j+" = "+prob);
        	}
        }
    }

	public static void llenarSoborno(Votantes[] votantes) {
        for (int i = 0; i < votantes.length; i++) {
            int so = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el Soborno del votante " + (i + 1)));
            Soborno soborno = new Soborno(so);
            System.out.println("Soborno votante. #"+i+" = "+so);
        }
    }
	
	public static void mostrarProbabilidad(Candidatos[] candidatos) {
        String candidato = "Cantidato | ", Probabilidad = "Probabilidad:  | ";
        for (int j = 0; j < candidatos.length; j++) {
        	candidato += String.valueOf(j + 1) + " |";
        	//Probabilidad = Probabilidad[j].getProbabilidad() + " |";
        }
        JOptionPane.showMessageDialog(null, "/*/*/Valor de probabildiad x candidato /*/*/ \n\n" + candidato + "\n" + Probabilidad + "\n");
    }

    public static void mostrarSoborno(Votantes votantes[]) {
        String votante = "Votante | ", Soborno = "Soborno:  | ";
        for (int i = 0; i < votantes.length; i++) {	
	        votante += String.valueOf(i + 1) + " |";
	        Soborno soborno = new Soborno();
	        //soborno += soborno[i].getSoborno();
        }
        JOptionPane.showMessageDialog(null, "/*/*/Valor de soborno x votante/*/*/ \n\n" + votante + "\n" + Soborno + "\n");
    }

    public static void operarSoborno() {
    	Soborno soborno = new Soborno();
    	Probabilidad probabilidad = new Probabilidad();
    	int ganador = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el n�mero del candidato que desea que gane..."));
    	System.out.println("Ganador: #"+ganador);
    	System.out.println(Arrays.asList(probabilidad));
    	int probTotal = 0;
        while (probTotal != 1) {
        	for(int i = 0; i < probabilidad.getProbabilidad(); i++){
        		probTotal += probabilidad.getProbabilidad();
        	}
        	mostrarResultado(probTotal);
        	System.out.println(probTotal);
        }
    }
    
    public static void mostrarResultado(int prob) {
    	JOptionPane.showMessageDialog(null, "El dinero que hay que darle a cada votante es:");
    	
    }

    public static void main(String[] args) {
    	int v = Integer.parseInt(JOptionPane.showInputDialog("Numero de Votantes:"));
    	int c = Integer.parseInt(JOptionPane.showInputDialog("Numero de Cantidatos:"));
    	        
        Votantes votantes[] = new Votantes[v];
        Candidatos candidatos[] = new Candidatos[c];
        
        llenarProbabilidad(votantes, candidatos);
        //mostrarProbabilidad(candidatos);
        llenarSoborno(votantes);
        //mostrarSoborno(votantes);
        operarSoborno();
    }

}
