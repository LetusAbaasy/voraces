import java.util.Arrays;

public class voraz01
{
	public static int m;         // capacidad mochila
	static String[]nombre;// nombre elemento
	static int[]peso;     // peso elemento
	static int[]beneficio;// beneficio/kg de cada elemento

	public static void main (String arg [] )
	{
		int m= 100;
		
		int n=8; // n�mero elementos 
		nombre=new String[n];
		nombre[0]="Hierro";
		nombre[1]="Carbon";
		nombre[2]="Cobre";
		nombre[3]="Zinc";
		nombre[4]="Plomo";
		nombre[5]="Niquel";
		nombre[6]="Platino";
		nombre[7]="Aluminio";

		beneficio=new int[n];
		// SE SUPONE YA ORDENADO DECRECIENTEMENTE POR BENEFICIO 
		beneficio[0]=90;
		beneficio[1]=85;
		beneficio[2]=82;
		beneficio[3]=80;
		beneficio[4]=70;
		beneficio[5]=68;
		beneficio[6]=65;
		beneficio[7]=60;

		peso=new int[n];
		peso[0]=45;
		peso[1]=30;
		peso[2]=25;
		peso[3]=60;
		peso[4]=50;
		peso[5]=40;
		peso[6]=80;
		peso[7]=100;

		System.out.println ("CAPACIDAD DE LA MOCHILA= "+m+" Toneladas");
		System.out.println ("SE CARGA LO SIGUIENTE:");

		int[][] valorMochila = Mochila(peso,beneficio,m);

		System.out.println ();
		System.out.println ("BENEFICIO DE LA CARGA= "+valorMochila);
	} 

	static void mostrarElementos()
	{
		System.out.print("Mineral: ");
		for (int i= 0; i<nombre.length; i++)
		{
			System.out.print(nombre);
		}

	}
	
	public static int[][] Mochila(int[] pesos, int[] beneficios, int capacidad){   
		//Creamos la matriz de devoluciones    
		int[][]  matriz_mochila = new int[pesos.length+1][capacidad+1];
		//Rellenamos la 1� fila de ceros   
		for(int i = 0; i <= capacidad; i++)  
			matriz_mochila[0][i] = 0;    //Rellenamos la 1� columna de ceros 
		for(int i = 0; i <= pesos.length; i++)           
			matriz_mochila[i][0] = 0;           
		for(int j = 1; j <= pesos.length ; j++)          
			for(int c = 1; c <= capacidad; c++){       
				if(c <  pesos[j-1] ){           
					matriz_mochila[j][c] = matriz_mochila[j-1][c];   
					}else{               
						if(matriz_mochila[j-1][c] > matriz_mochila[j-1][c-pesos[j-1]]+ beneficios[j-1]){    
							matriz_mochila[j][c] = matriz_mochila[j-1][c];           
							}else{                
								matriz_mochila[j][c] = matriz_mochila[j-1][c-pesos[j-1]]+beneficios[j-1]; 
								}           
						}     
				
				}       
		System.out.println(Arrays.deepToString(matriz_mochila));
		return matriz_mochila;    
	}
}